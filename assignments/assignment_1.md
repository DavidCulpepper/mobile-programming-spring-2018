**CSC 490 - Mobile Application Development**

**Assignment 1**

**Summary:** Build an Android application that will display an image full screen. A mechanism should be available to display information about the app.

**Due:** Wednesday, 1/31/2018 at 6:00 PM

**Rubric:** 50 Points

1. (15 pts) When I launch the app, then the screen should show a full screen view of an image.
2. (15 pts) When I use the mechanism to show the about info, I should see information about the name of the app, the author, and the current version.
3. (10 pts) Provide a README.md file in the root of your project that details the design decisions that you make, suggests alternatives that you considered, and explains your reasoning for choosing what you did.
4. (10 pts) Impress me.

**Submission:** You will submit your project by creating a git repository and pushing it to your BitBucket repository (that you have granted me access) by the due date and time. 