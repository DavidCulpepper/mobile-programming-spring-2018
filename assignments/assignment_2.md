**CSC 490 - Mobile Application Development**

**Assignment 2**

**Summary:** Build an Android application that will display a meme full screen. A mechanism should be provided to edit the top text, bottom text, and image used for the meme. All functionality from Assignemt 1 should remain.

**Due:** Wednesday, 2/28/2018 at 6:00 PM

**Rubric:** 100 Points

1. (10 pts) Don't break existing functionality (Update the version in your about).
1. (25 pts) When I launch the app, I should be greeted with the image of a meme with an image and top text and bottom text.
1. (25 pts) When I use the mechanism to edit the top text, bottom text, and image, I should see the selections populated with the existing values.
1. (20 pts) When I save my changes after I edit the bottom text, top text, or image, I should be returned to the original image with my changes now shown.
1. (10 pts) Add to your README.md file in the root of your project that details the design decisions that you make, suggests alternatives that you considered, and explains your reasoning for choosing what you did.
1. (10 pts) Impress me.

**Submission:** You will submit your project by creating a git repository and pushing it to your BitBucket repository (that you have granted me access) by the due date and time. 